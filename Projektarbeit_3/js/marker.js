let map, data, tmpHeat
let heatMap = L.layerGroup()
let windStations = L.layerGroup()
let hm_temp, hm_PM10, hm_PM2_5
let infoPM2_5 = L.control({position: 'bottomleft'}),
    infoPM10 = L.control({position: 'bottomleft'}),
    infoTemp = L.control({position: 'bottomleft'})
let legendPM2_5 = L.control({position: 'bottomright'}),
    legendPM10 = L.control({position: 'bottomright'}),
    legendTemp = L.control({position: 'bottomright'})

let markerClusterPM10 = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: getIconCreateFunction_pm10(),
    //Disable all of the defaults:
    spiderfyOnMaxZoom: true, showCoverageOnHover: true, zoomToBoundsOnClick: true
})
let markerClusterPM2_5 = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: getIconCreateFunction_pm25(),
    //Disable all of the defaults:
    spiderfyOnMaxZoom: true, showCoverageOnHover: true, zoomToBoundsOnClick: true
})
let markerClusterTemperature = L.markerClusterGroup({
    maxClusterRadius: 120,
    iconCreateFunction: getIconCreateFunction_temp(),
    //Disable all of the defaults:
    spiderfyOnMaxZoom: true, showCoverageOnHover: true, zoomToBoundsOnClick: true
})

const stationsLegendHTML = `
                    <h6>Messwerte</h6>
                    <table><tr>
                        <td><div id="base_Legend"><div>42</div></div></td>
                        <td>Messstation</td>
                    </tr><tr>
                        <td><div id="cluster_Legend"><div>42</div></div></td>
                        <td>Gruppierung</td>
                    </tr>
                    </table>`
const windLegendHTML = `<hr>
                   <h6>Wind</h6>
                   <table><tr>
                        <td><div id="wind_Legend">
                                <svg xmlns="http://www.w3.org/2000/svg" width="1.7rem" height="1.7rem" viewBox="0 0 387 604" preserveAspectRatio="xMidYMid meet">
                                    <g transform="translate(0,604) scale(0.1,-0.1)" fill="hsla(100, 100%, 39%, 1)" stroke="none">
                                        <path d="M1927 6020 c-12 -30 -707 -2197 -1340 -4180 -552 -1727 -577 -1805 -577 -1818 0 -14 176 103 1190 788 393 266 722 484 732 484 9 1 233 -145 497 -324 1173 -792 1435 -969 1438 -966 1 1 -40 137 -93 302 -387 1214 -1818 5666 -1838 5719 -2 6 -6 3 -9 -5z"/>
                                    </g>
                                </svg>
                            </div><div id="windLetter_Legend">2 km/h</div>
                        <td>Windstation mit:<br>- Richtung<br>- Geschwindigkeit</td>
                    </tr></table>`

$(document).ready(function () {
    initialize()
})

function initialize() {
    initializeMap()
    initHeatMap()
    initMapControls()

    updateData()
}

async function updateData(){
    console.log("Update Data")
    document.getElementById("status").innerHTML = "<b style='color: red'>Lädt Daten</b>"
    let tmpCluster
    if (map.hasLayer(markerClusterTemperature)) {
        tmpCluster = markerClusterTemperature
        tmpHeat = hm_temp
    } else if (map.hasLayer(markerClusterPM10)) {
        tmpCluster = markerClusterPM10
        tmpHeat = hm_PM10
    } else {
        tmpCluster = markerClusterPM2_5
        tmpHeat = hm_PM2_5
    }

    await getAirData()
    initializeWindLayer()

    markerClusterPM2_5.remove()
    markerClusterPM10.remove()
    markerClusterTemperature.remove()

    tmpCluster.addTo(map)

    setTimeout(updateData, 60000)
}

function initializeMap() {
    map = L.map('map', {maxZoom: 18, layers: [markerClusterPM2_5]}).setView([48.7755, 9.1784], 14);

    const traffic = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution:  'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> Simon Kellner & Tobias Wieck,<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/navigation-preview-day-v2',
        accessToken: 'pk.eyJ1IjoidG9zaS1nZW92aXMiLCJhIjoiY2trNGtocXJkMTR0ODJ3b2J0amxnd2JubyJ9.oGuMltMWqBBbK5rs5xk-cg'
    })
    const osm = new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://openstreetmap.org">OpenStreetMap</a>     Simon Kellner & Tobias Wieck',
        minZoom: 2,
        maxZoom: 18,
    })
    map.addLayer(traffic)

    const clusters = {
        "PM2.5": markerClusterPM2_5,
        "PM10": markerClusterPM10,
        "Temperature": markerClusterTemperature,
    }
    const checkLayer = {
        "HeatMap": heatMap,
        "Wind": windStations,
    }
    L.control.layers(clusters, checkLayer).addTo(map);
    map.on('baselayerchange', function (e) {
        switch (e.name) {
            case "Temperature":
                heatMap.clearLayers()
                hm_temp.addTo(heatMap)
                map.removeControl(infoPM2_5)
                map.removeControl(infoPM10)
                map.removeControl(legendPM2_5)
                map.removeControl(legendPM10)
                map.addControl(infoTemp)
                map.addControl(legendTemp)
                break
            case "PM10":
                heatMap.clearLayers()
                hm_PM10.addTo(heatMap)
                map.removeControl(infoPM2_5)
                map.removeControl(infoTemp)
                map.removeControl(legendPM2_5)
                map.removeControl(legendTemp)
                map.addControl(infoPM10)
                map.addControl(legendPM10)
                break
            case "PM2.5":
                heatMap.clearLayers()
                hm_PM2_5.addTo(heatMap)
                map.removeControl(infoPM10)
                map.removeControl(infoTemp)
                map.removeControl(legendPM10)
                map.removeControl(legendTemp)
                map.addControl(infoPM2_5)
                map.addControl(legendPM2_5)
        }
    })
}

function initHeatMap() {
    hm_PM2_5 = L.idwLayer([],
        {
            opacity: 0.5,
            cellSize: 20,
            exp: 3,
            max: 25,
            gradient: {0: '#15b11e', 0.5: '#ff9900',1: '#ff3100'}
        })
    hm_PM10 = L.idwLayer([],
        {
            opacity: 0.5,
            cellSize: 20,
            exp: 3,
            max: 50,
            gradient: {0: '#15b11e', 0.5: '#ff9900',1: '#ff3100'}
        })
    hm_temp = L.idwLayer([],
        {
            opacity: 0.5,
            cellSize: 20,
            exp: 5,
            max: 80,
            gradient: {0: '#0000ff',0.7: '#ff9a00', 1: '#ff0000'}
        })
}

function initMapControls() {
    infoPM2_5.onAdd = function () {
        this._div = L.DomUtil.create('div', 'info')
        this._div.innerHTML = `
            <h4>Feinstaub - PM<sub>2.5</sub></h4>
            <h6>In <b>μg/m<sup>3</sup></b></h6>
            <h6> <strong>Grenzwerte nach WHO:</strong></h6>
            <div class="legend legend_upper" style="background: #109618;">Keine Überschreitung</div>
            <div class="legend" style="background: #f69400;">Über Jahresgrenze</div>
            <div class="legend legend_lower" style="background: #DC3912;"; >Über Tagesgrenze</div>
            `
        return this._div
    }
    infoPM10.onAdd = function () {
        this._div = L.DomUtil.create('div', 'info')
        this._div.innerHTML = `
            <h4>Grobe Partikel - PM<sub>10</sub></h4>
            <h6>In <b>μg/m<sup>3</sup></b></h6>
            <h6> <strong>Grenzwerte nach WHO:</strong></h6>
            <div class="legend legend_upper" style="background: #109618;">Keine Überschreitung</div>
            <div class="legend" style="background: #f69400;">Über Jahresgrenze</div>
            <div class="legend legend_lower" style="background: #DC3912;"; >Über Tagesgrenze</div>
            `
        return this._div
    }
    infoTemp.onAdd = function () {
        this._div = L.DomUtil.create('div', 'info')
        this._div.innerHTML = `
            <h4>Temperatur</h4>In <b>°C</b>
            <h6><strong>Temperaturbereiche:</strong></h6>
            <div class="legend legend_upper" style="background: #006fc3;">Kalt</div>
            <div class="legend" style="background: #f69400;">Gemäßigt</div>
            <div class="legend legend_lower" style="background: #DC3912;"; >Heiß</div>
            `
        return this._div
    }
    map.addControl(infoPM2_5)

    legendPM2_5.onAdd = function () {
        let div = L.DomUtil.create('div', 'info legendRight'),
            description = ["0 - 10", "10 - 25", "> 25"],
            colors = ["#109618", "#FF9900", "#DC3912"]
        div.innerHTML += stationsLegendHTML
        for (let i = 2; i >= 0; i--) {
            div.innerHTML += `<i style="background: ${colors[i]}"></i>${description[i]} μg/m3<br>`
        }
        div.innerHTML +=
            `<hr><h6>HeatMap</h6>
                    <table><tr><td>
                        <i id="heatMapI" style="background: linear-gradient(to bottom, #ff3100, #ff9900 50%, #15b11e);"></i>
                    </td><td>
                        <p id="heatMapTextRows">> 25 μg/m3<br><br>0 μg/m3</p>
                    </td></tr></table>`
        div.innerHTML += windLegendHTML
        return div
    }
    legendPM10.onAdd = function () {
        let div = L.DomUtil.create('div', 'info legendRight'),
            description = ["0 - 20", "20 - 50", "> 50"],
            colors = ["#109618", "#FF9900", "#DC3912"]
        div.innerHTML += stationsLegendHTML
        for (let i = 2; i >= 0; i--) {
            div.innerHTML += `<i style="background: ${colors[i]}"></i>${description[i]} μg/m3<br>`
        }
        div.innerHTML +=
            `<hr><h6>HeatMap</h6>
                    <table><tr><td>
                        <i id="heatMapI" style="background: linear-gradient(to bottom, #ff3100, #ff9900 50%, #15b11e);"></i>
                    </td><td>
                        <p id="heatMapTextRows">> 50 μg/m3<br><br>0 μg/m3</p>
                    </td></tr></table>`
        div.innerHTML += windLegendHTML
        return div
    }
    legendTemp.onAdd = function () {
        let div = L.DomUtil.create('div', 'info legendRight'),
            description = ["-40 - 5", "5 - 30", "> 30"],
            colors = ["#006fc3", "#f69400", "#DC3912"]
        div.innerHTML += stationsLegendHTML
        for (let i = 2; i >= 0; i--) {
            div.innerHTML += `<i style="background: ${colors[i]}"></i>${description[i]} °C<br>`
        }
        div.innerHTML +=
            `<hr><h6>HeatMap</h6>
                    <table><tr><td>
                        <i id="heatMapI" style="background: linear-gradient(to bottom, #ff0000, #ff9a00 30%, #0000ff);"></i>
                    </td><td>
                        <p id="heatMapTextRows">> 40 °C<br>0 °C<br>< -40 °C</p>
                    </td></tr></table>`
        div.innerHTML += windLegendHTML
        return div
    }
    map.addControl(legendPM2_5)
}

function getAirData() {
    fetch('https://data.sensor.community/airrohr/v1/filter/area=48.7785,9.18,9')
        .then(function (response) {
            return response.text()
        })
        .then(function (text) {
            markerClusterPM2_5.clearLayers()
            markerClusterPM10.clearLayers()
            markerClusterTemperature.clearLayers()
            heatMap.clearLayers()

            console.log('Request successful')
            data = JSON.parse(text)
            filterData()
            reduceMultipleData()
            setMarkers()
            heatMap.clearLayers()
            tmpHeat.addTo(heatMap)
            calculateAverages()
            updateStatus()
        })
        .catch(function (error) {
            console.log('Request failed', error)
        });
}

function initializeWindLayer() {
    windStations.clearLayers()
    const zips = ["70599", "70569", "70499", "70378", "70193", "70188", "70327", "70469", "70619"]
    for (const zip of zips) {
        fetch(`http://api.openweathermap.org/data/2.5/weather?zip=${zip},de&appid=05e480682aaa0d932a08704b5c573d78`)
            .then(function (response) {
                return response.text()
            })
            .then(function (text) {
                setWindMarker(JSON.parse(text))
            })
            .catch(function (error) {
                console.log('Request failed', error)
            });
    }
}

function setMarkers() {
    for (let ele in data) {
        for (let key of Object.keys(data[ele].sensordatavalues)) {
            setMarker(ele,
                data[ele].location.latitude,
                data[ele].location.longitude,
                data[ele].sensordatavalues[key],
                key,
                data[ele].timestamp)
        }
    }
}

function setMarker(id, lat, long, value, value_type, time) {
    const date = new Date(time)
    date.setHours(date.getHours() + 1)
    const hours = date.getHours() + ":" + date.getMinutes()

    let marker = L.marker([lat, long], {icon: createIcon(value, value_type)})
    marker.value = value
    marker.value_type = value_type
    switch (value_type) {
        case "P1":
            marker.bindPopup("<b>Station: " + id + "</b><br>" + value + " µg/m³<br>" + hours + " Uhr")
            markerClusterPM10.addLayer(marker)
            hm_PM10._latlngs.push([lat, long, value])
            break
        case "P2":
            marker.bindPopup("<b>Station: " + id + "</b><br>" + value + " µg/m³<br>" + hours + " Uhr")
            markerClusterPM2_5.addLayer(marker)
            hm_PM2_5._latlngs.push([lat, long, value])
            break
        case "temperature":
            marker.bindPopup("<b>Station: " + id + "</b><br>" + value + " °C<br>" + hours + " Uhr")
            markerClusterTemperature.addLayer(marker)
            hm_temp._latlngs.push([lat, long, value + 40])
            break
        default:
            return
    }

}

function setWindMarker(windData) {
    const speed = Math.round((windData.wind.speed * 3.6) * 10) / 10,
        direction = windData.wind.deg
    let marker = L.marker([windData.coord.lat, windData.coord.lon], {icon: createWindIcon(speed, direction)})
    marker.value = speed
    marker.value_type = direction
    marker.addTo(windStations)
}

function reduceMultipleData() {
    let result = data.reduce(function (acc, obj) {
        acc[obj.sensor.id] = acc[obj.sensor.id] || []
        if (acc[obj.sensor.id].length === 0) {
            acc[obj.sensor.id] = obj
        } else {
            obj.sensordatavalues.forEach(ele =>
                acc[obj.sensor.id].sensordatavalues.push(ele))
        }
        return acc
    }, {})
    for (let ele in result) {
        result[ele].sensordatavalues.reduce((a, b) => {
            result[ele].sensordatavalues[a.value_type] = result[ele].sensordatavalues[a.value_type] || []
            result[ele].sensordatavalues[b.value_type] = result[ele].sensordatavalues[b.value_type] || []

            if (result[ele].sensordatavalues[a.value_type].length === 0) {
                result[ele].sensordatavalues[a.value_type].push(a.value)
            }
            result[ele].sensordatavalues[b.value_type].push(b.value)
            return a
        })
    }
    for (let ele in result) {
        for (let key of Object.keys(result[ele].sensordatavalues)) {
            if (isNaN(parseInt(key, 10))) {
                const sum = result[ele].sensordatavalues[key].reduce((a, b) => Number.parseFloat(a) + Number.parseFloat(b), 0)
                let avg = (sum / result[ele].sensordatavalues[key].length)
                avg = Math.round(avg * 10) / 10
                result[ele].sensordatavalues[key] = avg
            } else {
                result[ele].sensordatavalues.shift()
            }
        }

    }
    data = result
}

function filterData() {
    data = data.filter(ele => ele.location.indoor === 0)
        .filter(ele => {
            const value = ele.sensordatavalues[0].value
            switch (ele.sensordatavalues[0].value_type) {
                case "temperature":
                    return value > -20 && value < 40
                case "P1":
                    return value > 0 && value < 300
                case "durP1":
                    return false
            }
        })
}

function calculateAverages() {
    let p1 = [], p2 = [], temp = []
    for (let ele in data) {
        for (let type in data[ele].sensordatavalues) {
            switch (type) {
                case "P1":
                    p1.push(data[ele].sensordatavalues[type])
                    break
                case "P2":
                    p2.push(data[ele].sensordatavalues[type])
                    break
                case "temperature":
                    temp.push(data[ele].sensordatavalues[type])
                    break
            }
        }
    }
    let sum = p1.reduce((a, b) => a + b, 0)
    let avg = (sum / p1.length)
    avg = Math.round(avg * 10) / 10
    sessionStorage.setItem("PM10", avg.toString())

    sum = p2.reduce((a, b) => a + b, 0)
    avg = (sum / p1.length)
    avg = Math.round(avg * 10) / 10
    sessionStorage.setItem("PM2_5", avg.toString())

    sum = temp.reduce((a, b) => a + b, 0)
    avg = (sum / p1.length)
    avg = Math.round(avg * 10) / 10
    sessionStorage.setItem("Temperature", avg.toString())
}

function updateStatus() {
    document.getElementById("status").innerHTML = `Daten von: <b>${new Date().toLocaleTimeString()}</b> Uhr`
}

function createIcon(value, value_type) {
    let color = "black"
    if (value_type === "P1") {
        //color = Math.round(-1 * value + 120)
        if (value <= 20) {
            color = "#109618"
        } else if (value <= 50) {
            color = "#f69400"
        } else if (value > 50) {
            color = "#DC3912"
        } else {
            color = "pink"
        }
    } else if (value_type === "P2") {
        //color = Math.round(-1 * value + 120)
        if (value <= 10) {
            color = "#109618"
        } else if (value <= 25) {
            color = "#f69400"
        } else if (value > 25) {
            color = "#DC3912"
        } else {
            color = "pink"
        }
    } else if (value_type === "temperature") {
        //color = Math.round(((1 / 12) * value * value) - ((20 / 3) * value) + (400 / 3))
        if (value <= 5) {
            color = "#006fc3"
        } else if (value <= 30) {
            color = "#f69400"
        } else if (value > 30) {
            color = "#DC3912"
        } else {
            color = "pink"
        }
    }
    const markerHtmlStylesF = `
	  background-color: ${color};
      border-style: solid;
      border-color: #363636;
      border-width: 1px;
      border-radius: 6px;
      font-size: large;
      text-align: center;
	  width: 40px;
	  height: 40px;
	  display: block;
	  left: -1.5rem;
	  top: -1.5rem;
	  position: relative;
	  border-radius: 3rem 3rem 3rem;
	  color: white;
	  `

    const markerHtmlStyles = `
      background-color: ${color};
    `

    return L.divIcon({
        className: "station-pin",
        iconAnchor: [25, 42],
        popupAnchor: [0, -42],
        html: `<div id="base" style="${markerHtmlStyles}"><div style="margin-top: 5.5px;">${value}</div></div>`,
        iconSize: L.point(50, 25)
    })
}

function createWindIcon(speed, direction) {
    let color = Math.round(((9 / 500) * speed * speed) - ((18 / 5) * speed) + 120)

    const style = `
                width: 55px;
                margin-top: 1px;
                font-size: 0.7rem;
                background: #f0f0f0;
                font-weight: bold;
                text-align: center;
                left: -5px;`

    const markerHtmlStyles = `transform: rotate(${direction}deg);`

    const svg = `<svg xmlns="http://www.w3.org/2000/svg"
                     width="1.7rem" height="1.7rem" viewBox="0 0 387 604"
                     preserveAspectRatio="xMidYMid meet">

            <g transform="translate(0,604) scale(0.1,-0.1)"
               fill="hsla(${color}, 100%, 39%, 1)" stroke="none">
                <path d="M1927 6020 c-12 -30 -707 -2197 -1340 -4180 -552 -1727 -577 -1805 -577 -1818 0 -14 176 103 1190 788 393 266 722 484 732 484 9 1 233 -145 497 -324 1173 -792 1435 -969 1438 -966 1 1 -40 137 -93 302 -387 1214 -1818 5666 -1838 5719 -2 6 -6 3 -9 -5z"/>
            </g>
        </svg>`
    return L.divIcon({
        className: "station-pin",
        iconAnchor: [10, 5],
        popupAnchor: [5, -10],
        html: `<div style="${markerHtmlStyles}">${svg}</div><div style="${style}">${speed} km/h</div>`,
        iconSize: L.point(35, 25)
    })
}

function getIconCreateFunction_pm25() {
    return function (cluster) {
        let markers = cluster.getAllChildMarkers();
        let avg = 0;
        for (let i = 0; i < markers.length; i++) {
            avg += parseFloat(markers[i].value);
        }
        avg = Math.round(avg / markers.length * 10) / 10
        let color
        if (avg <= 10) {
            color = "#109618"
        } else if (avg <= 25) {
            color = "#f69400"
        } else if (avg > 25) {
            color = "#DC3912"
        } else {
            color = "pink"
        }

        const style = `
        background-color: ${color};
        border-radius: 2rem;
        border-style: solid;
        border-color: #262626;
        border-width: 2px;
        font-size: large;
        text-align: center;
        color: white;
        line-height: 27px;
        height: 30px;
        `

        return L.divIcon({
            html: '<div style="' + style + '">' + avg + '</div>',
            className: 'mycluster',
            iconSize: L.point(50, 25)
        });
    };
}

function getIconCreateFunction_pm10() {
    return function (cluster) {
        let markers = cluster.getAllChildMarkers();
        let avg = 0;
        for (let i = 0; i < markers.length; i++) {
            avg += parseFloat(markers[i].value);
        }
        avg = Math.round(avg / markers.length * 10) / 10
        let color
        if (avg <= 20) {
            color = "#109618"
        } else if (avg <= 50) {
            color = "#f69400"
        } else if (avg > 50) {
            color = "#DC3912"
        } else {
            color = "pink"
        }

        const style = `
        background-color: ${color};
        border-radius: 2rem;
        border-style: solid;
        border-color: #262626;
        border-width: 2px;
        font-size: large;
        text-align: center;
        color: white;
        line-height: 27px;
        height: 30px;
        `

        return L.divIcon({
            html: '<div style="' + style + '">' + avg + '</div>',
            className: 'mycluster',
            iconSize: L.point(50, 25)
        });
    };
}

function getIconCreateFunction_temp() {
    return function (cluster) {
        let markers = cluster.getAllChildMarkers();
        let avg = 0;
        for (let i = 0; i < markers.length; i++) {
            avg += parseFloat(markers[i].value);
        }
        avg = Math.round(avg / markers.length * 10) / 10
        let color
        if (avg <= 5) {
            color = "#006fc3"
        } else if (avg <= 30) {
            color = "#f69400"
        } else if (avg > 30) {
            color = "#DC3912"
        } else {
            color = "pink"
        }

        const style = `
        background-color: ${color};
        border-radius: 2rem;
        border-style: solid;
        border-color: #262626;
        border-width: 2px;
        font-size: large;
        text-align: center;
        color: white;
        line-height: 27px;
        height: 30px;
        `

        return L.divIcon({
            html: '<div style="' + style + '">' + avg + '</div>',
            className: 'mycluster',
            iconSize: L.point(50, 25)
        });
    };
}
