let histAirData, histWindData

google.charts.load('current', {'packages':['gauge']});
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart_pm25);
google.charts.setOnLoadCallback(drawChart_pm10);
google.charts.setOnLoadCallback(drawChart_temp);
google.charts.setOnLoadCallback(drawChart_correlation);

$(document).ready(function () {
    if (!sessionStorage.getItem("PM10")) {
        window.location.replace("./Projektarbeit_3.html")
    }
})
function drawChart_pm25() {

    let data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['μg/m³', Number.parseFloat(sessionStorage.getItem("PM2_5"))]
    ]);

    let options = {
        width: 600, height: 200,
        greenFrom: 0, greenTo: 10,
        yellowFrom:10, yellowTo: 25,
        redFrom: 25, redTo: 50,
        min: 0, max: 50,
        majorTicks: ["0","","","","","50"],minorTicks: 10,
    };

    let chart = new google.visualization.Gauge(document.getElementById('chart_pm25'));

    chart.draw(data, options);
}

function drawChart_pm10() {

    let data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['μg/m³', Number.parseFloat(sessionStorage.getItem("PM10"))]
    ]);

    let options = {
        width: 600, height: 200,
        greenFrom: 0, greenTo: 20,
        yellowFrom:20, yellowTo: 50,
        redFrom: 50, redTo: 100,
        min: 0, max: 100,
        majorTicks: ["0","","","","","","","","","","100"],minorTicks: 5,
    };

    let chart = new google.visualization.Gauge(document.getElementById('chart_pm10'));

    chart.draw(data, options);
}

function drawChart_temp() {

    let data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['°C', Number.parseFloat(sessionStorage.getItem("Temperature"))]
    ]);

    let options = {
        width: 600, height: 200,
        greenFrom: -40, greenTo: 5, greenColor: '#006fc3',
        yellowFrom: 5, yellowTo: 30,
        redFrom: 30, redTo: 40,
        min: -40, max: 40,
        majorTicks: ["-40","","","","0","","","","40"],minorTicks: 5,
    };

    let chart = new google.visualization.Gauge(document.getElementById('chart_temp'));

    chart.draw(data, options);
}

async function drawChart_correlation() {

    await callApiForData()

    let dataArray = [['Hour', 'PM2,5 - Wert', 'Windgeschwindigkeit']]
    let rawAirArray = []
    let rawWindArray = []
    let day

    for (let ele in histAirData.list) {
        const date = new Date(histAirData.list[ele].dt * 1000)
        rawAirArray[date.getHours()] = histAirData.list[ele].components.pm2_5
        if (ele === '12') {
           day = date.toLocaleDateString()
        }
    }
    for (let ele in histWindData.hourly) {
        const date = new Date(histWindData.hourly[ele].dt * 1000)
        rawWindArray[date.getHours()] = histWindData.hourly[ele].wind_speed
    }
    for (let i = 0; i < 24; i++) {
        dataArray.push([i.toString(), rawAirArray[i], rawWindArray[i]])
    }

    let data = google.visualization.arrayToDataTable(dataArray)

    const options = {
        title: `Für den: ${day}`,
        curveType: 'function',
        legend: { position: 'bottom' }
    }

    const chart = new google.visualization.LineChart(document.getElementById('chart_correlation'));

    chart.draw(data, options);
}

async function callApiForData() {
    let dateNow = new Date(Date.now())
    let dateStart = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - 3)
    dateNow = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate() - 2)
    dateNow = Math.floor(dateNow.getTime() / 1000)
    dateStart = Math.floor(dateStart.getTime() / 1000)

    const requestHistAirData = async () => {
        const response = await fetch(`http://api.openweathermap.org/data/2.5/air_pollution/history?lat=48.775&lon=9.178&start=${dateStart}&end=${dateNow}&appid=05e480682aaa0d932a08704b5c573d78`)
        histAirData = await response.json()
    }
    const requestHistWindData = async () => {
        const response = await fetch(`https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=48.775&lon=9.178&dt=${dateStart}&appid=05e480682aaa0d932a08704b5c573d78`)
        histWindData = await response.json()
    }

    await requestHistAirData()
    await requestHistWindData()
}


window.onresize = function(){
    drawChart_correlation();
};