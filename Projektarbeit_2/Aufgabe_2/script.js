let map2;
const placesToEat = L.layerGroup(),
    placesToDrink = L.layerGroup(),
    placesToChill = L.layerGroup(),
    eatingIcon = L.icon({
        iconUrl: 'Aufgabe_2/icons/place_eating.png',
        iconSize: [30, 30], // size of the icon
        iconAnchor:   [15, 30], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -15] // point from which the popup should open relative to the iconAnchor
    }),
    drinkingIcon = L.icon({
        iconUrl: 'Aufgabe_2/icons/place_drinking.png',
        iconSize: [30, 30], // size of the icon
        iconAnchor:   [15, 30], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -15] // point from which the popup should open relative to the iconAnchor
    }),
    chillIcon = L.icon({
        iconUrl: 'Aufgabe_2/icons/place_to_chill.png',
        iconSize: [30, 30], // size of the icon
        iconAnchor:   [15, 30], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -15] // point from which the popup should open relative to the iconAnchor
    });

function initialize() {
    setMarkers();

    map2 = L.map('map_str', {layers: [placesToEat, placesToDrink, placesToChill]}).setView([48.7755, 9.1784], 14);

    const osm = new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://openstreetmap.org">OpenStreetMap</a>     Simon Kellner & Tobias Wieck',
        minZoom: 2,
        maxZoom: 18
    });
    map2.addLayer(osm);

    const overlays = {
        "Restaurants": placesToEat,
        "Bars und Clubs": placesToDrink,
        "Orte zum Verweilen": placesToChill
    };
    L.control.layers(null, overlays).addTo(map2);
}
function setMarkers(){
    setEatingMarkers();
    setDrinkingMarkers();
    setChillMarkers();
}
function setEatingMarkers(){
    L.marker([48.78099, 9.16966], {icon: eatingIcon})
        .bindPopup("<b>Mensa</b><br>Mensa f&uuml;r Uni, HFT, DHBW<br>Inside: Ganz ok, Burger in der MachBar ganz gut!")
        .addTo(placesToEat);
    L.marker([48.77555, 9.17147], {icon: eatingIcon})
        .bindPopup("<b>Birkan D&ouml;ner</b><br>Inside: Guter D&ouml;ner, mit Studenten/Sch&uuml;lerpreisen!")
        .addTo(placesToEat);
    L.marker([48.77965, 9.17838], {icon: eatingIcon})
        .bindPopup("<b>Hotalo</b><br>Leckere und preiswerte Nudelboxen")
        .addTo(placesToEat);
    L.marker([48.77948, 9.17798], {icon: eatingIcon})
        .bindPopup("<b>Food Lounge im K&ouml;nigsbau</b><br>Viele verschiedene Essen, f&uuml;r jeden was dabei<br>Tipp: B&uuml;ffeletti Burger")
        .addTo(placesToEat);
    L.marker([48.77073, 9.18204], {icon: eatingIcon})
        .bindPopup("<b>Alaturka D&ouml;ner</b><br>Bester D&ouml;ner deutschlands!<br>Inside: Lange Wartezeiten, aber es lohnt!")
        .addTo(placesToEat);

}
function setDrinkingMarkers(){
    L.marker([48.78040,9.17256], { icon: drinkingIcon})
        .bindPopup("<b>Block 4</b><br>Studenten Bar der HFT")
        .addTo(placesToDrink);
    L.marker([48.78050,9.17727], { icon: drinkingIcon})
        .bindPopup("<b>Palast der Republik</b><br>Outdoor Bar, deshalb nur wenns warm ist<br>Tipp: Gut um in den Abend zu starten")
        .addTo(placesToDrink);
    L.marker([48.77937,9.17702], { icon: drinkingIcon})
        .bindPopup("<b>Tequila Bar</b><br>Zu jedem Bier gibts ein Shot gratis!<br>Inside: Dienstag Studententag = Bier 3&euro;")
        .addTo(placesToDrink);
    L.marker([48.77734,9.17203], { icon: drinkingIcon})
        .bindPopup("<b>Jigger & Spoon</b><br>Versteckte Bar in ehmaligem Banktresor<br>Indide: Etwas teuer, aber gute Drinks und besondere Location")
        .addTo(placesToDrink);
    L.marker([48.77357,9.17727], { icon: drinkingIcon})
        .bindPopup("<b>Barviertel, Hans im Gl&uuml;ck-Brunnen</b><br>Viele Bars, perfekt f&uuml;r eine Bartour")
        .addTo(placesToDrink);
    L.marker([48.78218,9.17749], { icon: drinkingIcon})
        .bindPopup("<b>PURE Club</b><br>Hip-Hop und Mainstream-Floor<br>Inside: Vor 23 Uhr freier Eintritt und g&uuml;nstigere Getr&auml;nke!")
        .addTo(placesToDrink);
}
function setChillMarkers(){
    L.marker([48.7788,9.1793], { icon: chillIcon})
        .bindPopup("<b>Schlossplatz</b><br>Sch&ouml;ne Kulisse, allerdings sehr Mainstream")
        .addTo(placesToChill);
    L.marker([48.77967,9.18192], { icon: chillIcon})
        .bindPopup("<b>Eckensee</b><br>Blick auf die Oper und See. Nicht so voll wie der Schlossplatz")
        .addTo(placesToChill);
    L.marker([48.77781,9.18992], { icon: chillIcon})
        .bindPopup("<b>Eugensplatz</b><br>Sch&ouml;ner Blick auf die Stadt.<br>Perfekt mit Bier oder Eis vom 'Pinguin'")
        .addTo(placesToChill);
    L.marker([48.7812,9.17364], { icon: chillIcon})
        .bindPopup("<b>Stadtgarten</b><br>Entspanntes Ambiente und viele Studenten")
        .addTo(placesToChill);
    L.marker([48.76485,9.18208], { icon: chillIcon})
        .bindPopup("<b>Wei&szlig;enburg</b><br>Sch&ouml;ne Aussicht, aber dadurch auch steiler Aufstieg<br>")
        .addTo(placesToChill);
    L.marker([48.76798,9.16540], { icon: chillIcon})
        .bindPopup("<b>Biergarten Tschechen & S&ouml;hne</b><br>Sch&ouml;ne Aussicht, Bier und Essen. Was will man mehr!<br>")
        .addTo(placesToChill);
}