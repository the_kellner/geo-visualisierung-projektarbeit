
var map = L.map('map').setView([54, 17], 4);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGhla2VsbG5lciIsImEiOiJja2hkamxhNmUwNmZkMnNudml2ZDU5aDlyIn0.hqsiXke9UjfrC8uWdt2ygw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/light-v9',
    tileSize: 512,
    zoomOffset: -1
}).addTo(map);


// control that shows state info on hover
var info = L.control();

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
};

info.update = function (props) {
    this._div.innerHTML = '<h4>Europe Population Density</h4>' +  (props ?
        '<b>' + props.name + '</b><br />' + Math.round(props.density * 100) / 100  + ' people per kilometre<sup>2</sup> <br/> at a population of ' + props.pop_est + ' people'
        : 'Hover over a state');
};

info.addTo(map);

// get color depending on population density value
function getColor(density) {
    switch (true) {
        case (density > 400): return '#800026'
        case (density > 300): return '#BD0026'
        case (density > 200): return '#E31A1C'
        case (density > 150): return '#FC4E2A'
        case (density > 100): return '#FD8D3C'
        case (density > 75): return '#FEB24C'
        case (density > 50): return '#FED976'
        case (density > 25): return '#FFEDA0'
        case (density > 10): return '#fffeda'
        default: return 'rgb(255,254,238)'
    }
}

function style(feature) {
    return {
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7,
        fillColor: getColor(feature.properties.density)
    };
}

function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }

    info.update(layer.feature.properties);
}

var geojson;

function resetHighlight(e) {
    geojson.resetStyle(e.target);
    info.update();
}

function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}



//Add Legend
var legend = L.control({position: 'bottomright'});
legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
        grades = [10, 25, 50, 75, 100, 150, 200, 300, 400],
        labels = [],
        from, to;

    for (var i = 0; i < grades.length; i++) {
        from = grades[i];
        to = grades[i + 1];

        const transform = (number) => {
            if (number == undefined) {
                return 0
            }
            stringNumber = number + "";
            return stringNumber + " p/km<sup>2</sup>"
        }

        labels.push(
            '<i style="background:' + getColor(from + 1) + '"></i> ' +
            transform(from) + (transform(to) ? '&ndash;' + transform(to) : '+'));
    }

    div.innerHTML = labels.join('<br>');
    return div;
};
legend.addTo(map);

// Add data
$.getJSON('custom.geo.json', statesData => {
    statesData.features.map(feature => feature.properties.density = feature.properties.pop_est / (turf.area(feature) / 1_000_000))
    geojson = L.geoJson(statesData, {style: style, onEachFeature: onEachFeature}).addTo(map);
})