//@ignore BestPractices
const geoCodesEurope = [8, 20, 56, 40, 70, 100, 112, 191, 203, 208, 233, 246, 250, 276, 292, 300, 348, 352, 372, 380, 428, 438, 440, 442, 470, 492, 498, 499, 528, 578, 616, 620, 642, 643, 674, 688, 703, 705, 724, 752, 756, 804, 807, 826, 830, 833];
let countries_h2o = []
let countries_san = []

// Indicators: SH_SAN_SAFE; SH_H2O_SAFE

async function initializeH2O() {
    let requests = []
    for (let code of geoCodesEurope) {
        const url = "https://unstats.un.org/SDGAPI/v1/sdg/Series/SH_H2O_SAFE/GeoArea/" + code + "/DataSlice?dimensions=%5B%7Bname%3A%22Location%22%2Cvalues%3A%5B%22ALLAREA%22%5D%7D%5D&timePeriods=%5B2015%2C2016%2C2017%5D";
        requests.push(fetch(url).then(value => value.json()))
    }
    countries_h2o = await Promise.all(requests)
    countries_h2o.forEach(y => {
        if (y.dimensions.length === 0) {
            y.dimensions = JSON.parse('[{"value":"No Data available","timePeriodStart":"2015"},' +
                '{"value":"No Data available","timePeriodStart":"2016"},' +
                '{"value":"No Data available","timePeriodStart":"2017"}]')
        }
    })
    aufgabe3_h2o_15()
    aufgabe3_h2o_16()
    aufgabe3_h2o_17()
}
async function initializeSAN() {
    let requests = []
    for (let code of geoCodesEurope) {
        const url = "https://unstats.un.org/SDGAPI/v1/sdg/Series/SH_SAN_SAFE/GeoArea/" + code + "/DataSlice?dimensions=%5B%7Bname%3A%22Location%22%2Cvalues%3A%5B%22ALLAREA%22%5D%7D%5D&timePeriods=%5B2015%2C2016%2C2017%5D";
        requests.push(fetch(url).then(value => value.json()))
    }
    countries_san = await Promise.all(requests)
    countries_san.forEach(y => {
        if (y.dimensions.length === 0) {
            y.dimensions = JSON.parse('[{"value":"No Data available","timePeriodStart":"2015"},' +
                '{"value":"No Data available","timePeriodStart":"2016"},' +
                '{"value":"No Data available","timePeriodStart":"2017"}]')
        }
    })
    aufgabe3_san_15()
    aufgabe3_san_16()
    aufgabe3_san_17()
}

function getData(dataArray ,geoCode, year) {
    for (let country of dataArray) {
        if (country.geoAreaCode === geoCode) {
            for (let entry of country.dimensions) {
                if (entry.timePeriodStart === year) {
                    return Math.round(entry.value * 100) / 100
                }
            }
        }
    }
    return "No Data avalible"
}

$(document).ready(function () {
    initializeH2O()
    initializeSAN()
})

const aufgabe3_h2o_15 = () => {
    var map = L.map('h2o_2015').setView([54, 17], 4);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGhla2VsbG5lciIsImEiOiJja2hkamxhNmUwNmZkMnNudml2ZDU5aDlyIn0.hqsiXke9UjfrC8uWdt2ygw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/light-v9',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(map);


// control that shows state info on hover
    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    info.update = function (props) {
        this._div.innerHTML = '<h4>Proportion of population using safely<br> managed drinking water services</h4>' + (props ?
            '<b>' + props.name + '</b><br />' + props.h2o + "%"
            : 'Hover over a state');
    };

    info.addTo(map);

// get color depending on population density value
    function getColor(percent) {
        switch (true) {
            case (percent > 99.9):
                return '#800026'
            case (percent > 99):
                return '#BD0026'
            case (percent > 95):
                return '#E31A1C'
            case (percent > 90):
                return '#FC4E2A'
            case (percent > 85):
                return '#FD8D3C'
            case (percent > 80):
                return '#FEB24C'
            case (percent > 75):
                return '#FED976'
            case (percent > 70):
                return '#FFEDA0'
            case (percent > 60):
                return '#fffeda'
            default:
                return 'rgb(255,254,238)'
        }
    }

    function style(feature) {
        return {
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7,
            fillColor: getColor(feature.properties.h2o)
        };
    }

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    var geojson;

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        info.update();
    }

    function zoomToFeature(e) {
        map.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
    }


//Add Legend
    var legend = L.control({position: 'bottomright'});
    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 60, 70, 75, 80, 85, 90, 95, 99, 99.9],
            labels = [],
            from, to;

        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];

            const transform = (number) => {
                if (number == undefined) {
                    return 0
                }
                stringNumber = number + "";
                return stringNumber + "%"
            }

            labels.push(
                '<i style="background:' + getColor(from + 1) + '"></i> ' +
                transform(from) + (transform(to) ? '&ndash;' + transform(to) : '&ndash;100%'));
        }

        div.innerHTML = labels.join('<br>');
        return div;
    };
    legend.addTo(map);

// Add data
    $.getJSON('custom.geo.json', statesData => {
        statesData.features.map(feature => feature.properties.h2o = getData(countries_h2o, feature.properties.geoAreaCode, "2015"))
        geojson = L.geoJson(statesData, {style: style, onEachFeature: onEachFeature}).addTo(map);
    })
    $(document).ready(function(){
        $("input[type='button']").click(function(){
            setTimeout(function () {
                map.invalidateSize()
            }, 50);
        });
    });}
const aufgabe3_h2o_16 = () => {
    var map = L.map('h2o_2016').setView([54, 17], 4);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGhla2VsbG5lciIsImEiOiJja2hkamxhNmUwNmZkMnNudml2ZDU5aDlyIn0.hqsiXke9UjfrC8uWdt2ygw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/light-v9',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(map);


// control that shows state info on hover
    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    info.update = function (props) {
        this._div.innerHTML = '<h4>Proportion of population using safely<br> managed drinking water services</h4>' + (props ?
            '<b>' + props.name + '</b><br />' + props.h2o + "%"
            : 'Hover over a state');
    };

    info.addTo(map);

// get color depending on population density value
    function getColor(percent) {
        switch (true) {
            case (percent > 99.9):
                return '#800026'
            case (percent > 99):
                return '#BD0026'
            case (percent > 95):
                return '#E31A1C'
            case (percent > 90):
                return '#FC4E2A'
            case (percent > 85):
                return '#FD8D3C'
            case (percent > 80):
                return '#FEB24C'
            case (percent > 75):
                return '#FED976'
            case (percent > 70):
                return '#FFEDA0'
            case (percent > 60):
                return '#fffeda'
            default:
                return 'rgb(255,254,238)'
        }
    }

    function style(feature) {
        return {
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7,
            fillColor: getColor(feature.properties.h2o)
        };
    }

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    var geojson;

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        info.update();
    }

    function zoomToFeature(e) {
        map.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
    }


//Add Legend
    var legend = L.control({position: 'bottomright'});
    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 60, 70, 75, 80, 85, 90, 95, 99, 99.9],
            labels = [],
            from, to;

        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];

            const transform = (number) => {
                if (number == undefined) {
                    return 0
                }
                stringNumber = number + "";
                return stringNumber + "%"
            }

            labels.push(
                '<i style="background:' + getColor(from + 1) + '"></i> ' +
                transform(from) + (transform(to) ? '&ndash;' + transform(to) : '&ndash;100%'));
        }

        div.innerHTML = labels.join('<br>');
        return div;
    };
    legend.addTo(map);

// Add data
    $.getJSON('custom.geo.json', statesData => {
        statesData.features.map(feature => feature.properties.h2o = getData(countries_h2o, feature.properties.geoAreaCode, "2016"))
        geojson = L.geoJson(statesData, {style: style, onEachFeature: onEachFeature}).addTo(map);
    })
    $(document).ready(function(){
        $("input[type='button']").click(function(){
            setTimeout(function () {
                map.invalidateSize()
            }, 50);
        });
    });}
const aufgabe3_h2o_17 = () => {
    var map = L.map('h2o_2017').setView([54, 17], 4);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGhla2VsbG5lciIsImEiOiJja2hkamxhNmUwNmZkMnNudml2ZDU5aDlyIn0.hqsiXke9UjfrC8uWdt2ygw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/light-v9',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(map);

// control that shows state info on hover
    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    info.update = function (props) {
        this._div.innerHTML = '<h4>Proportion of population using safely<br> managed drinking water services</h4>' + (props ?
            '<b>' + props.name + '</b><br />' + props.h2o + "%"
            : 'Hover over a state');
    };

    info.addTo(map);

// get color depending on population density value
    function getColor(percent) {
        switch (true) {
            case (percent > 99.9):
                return '#800026'
            case (percent > 99):
                return '#BD0026'
            case (percent > 95):
                return '#E31A1C'
            case (percent > 90):
                return '#FC4E2A'
            case (percent > 85):
                return '#FD8D3C'
            case (percent > 80):
                return '#FEB24C'
            case (percent > 75):
                return '#FED976'
            case (percent > 70):
                return '#FFEDA0'
            case (percent > 60):
                return '#fffeda'
            default:
                return 'rgb(255,254,238)'
        }
    }

    function style(feature) {
        return {
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7,
            fillColor: getColor(feature.properties.h2o)
        };
    }

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    var geojson;

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        info.update();
    }

    function zoomToFeature(e) {
        map.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
    }

//Add Legend
    var legend = L.control({position: 'bottomright'});
    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 60, 70, 75, 80, 85, 90, 95, 99, 99.9],
            labels = [],
            from, to;

        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];

            const transform = (number) => {
                if (number == undefined) {
                    return 0
                }
                stringNumber = number + "";
                return stringNumber + "%"
            }

            labels.push(
                '<i style="background:' + getColor(from + 1) + '"></i> ' +
                transform(from) + (transform(to) ? '&ndash;' + transform(to) : '&ndash;100%'));
        }

        div.innerHTML = labels.join('<br>');
        return div;
    };
    legend.addTo(map);

// Add data
    $.getJSON('custom.geo.json', statesData => {
        statesData.features.map(feature => feature.properties.h2o = getData(countries_h2o, feature.properties.geoAreaCode, "2017"))
        geojson = L.geoJson(statesData, {style: style, onEachFeature: onEachFeature}).addTo(map);
    })
    $(document).ready(function(){
        $("input[type='button']").click(function(){
            setTimeout(function () {
                map.invalidateSize()
            }, 50);
        });
    });}
const aufgabe3_san_15 = () => {
    var map = L.map('san_2015').setView([54, 17], 4);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGhla2VsbG5lciIsImEiOiJja2hkamxhNmUwNmZkMnNudml2ZDU5aDlyIn0.hqsiXke9UjfrC8uWdt2ygw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/light-v9',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(map);

// control that shows state info on hover
    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    info.update = function (props) {
        this._div.innerHTML = '<h4>Proportion of population using safely<br> managed sanitation services</h4>' + (props ?
            '<b>' + props.name + '</b><br />' + props.san + "%"
            : 'Hover over a state');
    };

    info.addTo(map);

// get color depending on population density value
    function getColor(percent) {
        switch (true) {
            case (percent > 99):
                return '#800026'
            case (percent > 95):
                return '#BD0026'
            case (percent > 90):
                return '#E31A1C'
            case (percent > 80):
                return '#FC4E2A'
            case (percent > 70):
                return '#FD8D3C'
            case (percent > 60):
                return '#FEB24C'
            case (percent > 50):
                return '#FED976'
            case (percent > 30):
                return '#FFEDA0'
            case (percent > 10):
                return '#fffeda'
            default:
                return 'rgb(255,254,238)'
        }
    }

    function style(feature) {
        return {
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7,
            fillColor: getColor(feature.properties.san)
        };
    }

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    var geojson;

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        info.update();
    }

    function zoomToFeature(e) {
        map.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
    }

//Add Legend
    var legend = L.control({position: 'bottomright'});
    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 10, 30, 50, 60, 70, 80, 90, 95, 99],
            labels = [],
            from, to;

        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];

            const transform = (number) => {
                if (number == undefined) {
                    return 0
                }
                stringNumber = number + "";
                return stringNumber + "%"
            }

            labels.push(
                '<i style="background:' + getColor(from + 1) + '"></i> ' +
                transform(from) + (transform(to) ? '&ndash;' + transform(to) : '&ndash;100%'));
        }

        div.innerHTML = labels.join('<br>');
        return div;
    };
    legend.addTo(map);

// Add data
    $.getJSON('custom.geo.json', statesData => {
        statesData.features.map(feature => feature.properties.san = getData(countries_san, feature.properties.geoAreaCode, "2015"))
        geojson = L.geoJson(statesData, {style: style, onEachFeature: onEachFeature}).addTo(map);
    })

    $(document).ready(function(){
        $("input[type='button']").click(function(){
            setTimeout(function () {
                map.invalidateSize()
            }, 50);
        });
    });}
const aufgabe3_san_16 = () => {
    var map = L.map('san_2016').setView([54, 17], 4);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGhla2VsbG5lciIsImEiOiJja2hkamxhNmUwNmZkMnNudml2ZDU5aDlyIn0.hqsiXke9UjfrC8uWdt2ygw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/light-v9',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(map);

// control that shows state info on hover
    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    info.update = function (props) {
        this._div.innerHTML = '<h4>Proportion of population using safely<br>managed sanitation services</h4>' + (props ?
            '<b>' + props.name + '</b><br />' + props.h2o + "%"
            : 'Hover over a state');
    };

    info.addTo(map);

// get color depending on population density value
    function getColor(percent) {
        switch (true) {
            case (percent > 99):
                return '#800026'
            case (percent > 95):
                return '#BD0026'
            case (percent > 90):
                return '#E31A1C'
            case (percent > 80):
                return '#FC4E2A'
            case (percent > 70):
                return '#FD8D3C'
            case (percent > 60):
                return '#FEB24C'
            case (percent > 50):
                return '#FED976'
            case (percent > 30):
                return '#FFEDA0'
            case (percent > 10):
                return '#fffeda'
            default:
                return 'rgb(255,254,238)'
        }
    }

    function style(feature) {
        return {
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7,
            fillColor: getColor(feature.properties.h2o)
        };
    }

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    var geojson;

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        info.update();
    }

    function zoomToFeature(e) {
        map.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
    }

//Add Legend
    var legend = L.control({position: 'bottomright'});
    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 10, 30, 50, 60, 70, 80, 90, 95, 99],
            labels = [],
            from, to;

        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];

            const transform = (number) => {
                if (number == undefined) {
                    return 0
                }
                stringNumber = number + "";
                return stringNumber + "%"
            }

            labels.push(
                '<i style="background:' + getColor(from + 1) + '"></i> ' +
                transform(from) + (transform(to) ? '&ndash;' + transform(to) : '&ndash;100%'));
        }

        div.innerHTML = labels.join('<br>');
        return div;
    };
    legend.addTo(map);

// Add data
    $.getJSON('custom.geo.json', statesData => {
        statesData.features.map(feature => feature.properties.h2o = getData(countries_san, feature.properties.geoAreaCode, "2016"))
        geojson = L.geoJson(statesData, {style: style, onEachFeature: onEachFeature}).addTo(map);
    })
    $(document).ready(function(){
        $("input[type='button']").click(function(){
            setTimeout(function () {
                map.invalidateSize()
            }, 50);
        });
    });}
const aufgabe3_san_17 = () => {
    var map = L.map('san_2017').setView([54, 17], 4);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoidGhla2VsbG5lciIsImEiOiJja2hkamxhNmUwNmZkMnNudml2ZDU5aDlyIn0.hqsiXke9UjfrC8uWdt2ygw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/light-v9',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(map);

// control that shows state info on hover
    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    info.update = function (props) {
        this._div.innerHTML = '<h4>Proportion of population using safely<br> managed sanitation services</h4>' + (props ?
            '<b>' + props.name + '</b><br />' + props.san + "%"
            : 'Hover over a state');
    };

    info.addTo(map);

// get color depending on population density value
    function getColor(percent) {
        switch (true) {
            case (percent > 99):
                return '#800026'
            case (percent > 95):
                return '#BD0026'
            case (percent > 90):
                return '#E31A1C'
            case (percent > 80):
                return '#FC4E2A'
            case (percent > 70):
                return '#FD8D3C'
            case (percent > 60):
                return '#FEB24C'
            case (percent > 50):
                return '#FED976'
            case (percent > 30):
                return '#FFEDA0'
            case (percent > 10):
                return '#fffeda'
            default:
                return 'rgb(255,254,238)'
        }
    }

    function style(feature) {
        return {
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7,
            fillColor: getColor(feature.properties.san)
        };
    }

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature.properties);
    }

    var geojson;

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        info.update();
    }

    function zoomToFeature(e) {
        map.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
    }

//Add Legend
    var legend = L.control({position: 'bottomright'});
    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 10, 30, 50, 60, 70, 80, 90, 95, 99],
            labels = [],
            from, to;

        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];

            const transform = (number) => {
                if (number == undefined) {
                    return 0
                }
                stringNumber = number + "";
                return stringNumber + "%"
            }

            labels.push(
                '<i style="background:' + getColor(from + 1) + '"></i> ' +
                transform(from) + (transform(to) ? '&ndash;' + transform(to) : '&ndash;100%'));
        }

        div.innerHTML = labels.join('<br>');
        return div;
    };
    legend.addTo(map);

// Add data
    $.getJSON('custom.geo.json', statesData => {
        statesData.features.map(feature => feature.properties.san = getData(countries_san, feature.properties.geoAreaCode, "2017"))
        geojson = L.geoJson(statesData, {style: style, onEachFeature: onEachFeature}).addTo(map);
    })

    $(document).ready(function(){
        $("input[type='button']").click(function(){
            setTimeout(function () {
                map.invalidateSize()
            }, 50);
        });
    });
}

const ids = ["h2o_2015","h2o_2016","h2o_2017","san_2015","san_2016","san_2017"]

$(document).ready(function(){
    $("input[type='button']").click(function(){
        var radioValueData = $("input[name='data']:checked").val();
        var radioValueYear = $("input[name='year']:checked").val();
        if(radioValueData || radioValueYear){
            console.log("Your are a - " + radioValueData + " " + radioValueYear);
            ids.forEach(id => {
                document.getElementById(id).classList.add("d-none")
            })
            document.getElementById(radioValueData + "_" + radioValueYear).classList.remove("d-none")
        }
    });
});
