$(document).ready(function () {
    $("#myToast").toast('show');
});

$(document).ready(function () {
    $('#carousel').on('slid.bs.carousel', function () {
        drawPieYear();
        drawPieAccumulated();
    })
});

google.charts.load('current', {'packages': ['corechart', 'scatter']});

google.charts.setOnLoadCallback(drawPieYear);
google.charts.setOnLoadCallback(drawPieAccumulated);
google.charts.setOnLoadCallback(drawLine);
google.charts.setOnLoadCallback(drawScatter);
google.charts.setOnLoadCallback(drawBubble);

function drawPieYear() {
    let data = google.visualization.arrayToDataTable([
        ['Year', 'Jan-Dec'],
        ['2012', 625.88],
        ['2013', 731.03],
        ['2014', 763.83],
    ]);

    var options = {
        //colors: ['#fd0000','#43fa00','#0040ff'],
        hAxis: {title: 'Monat', titleTextStyle: {color: '#333333'}},
        vAxis: {
            minValue: 0,
            title: 'MWh',
        },
    };

    let chart = new google.visualization.PieChart(document.getElementById('chart_div_pieYear'));
    chart.draw(data, options);
}

function drawPieAccumulated() {
    var data = google.visualization.arrayToDataTable([
        ['Year', '2012-2014'],
        ['Januar', 386.73],
        ['Februar', 416.78],
        ['März', 262.68],
        ['April', 174.31],
        ['Mai', 65.32],
        ['Juni', 30.17],
        ['Juli', 0],
        ['August', 0],
        ['September', 39.14],
        ['Oktober', 153.25],
        ['November', 243.97],
        ['Dezember', 373.71],

    ]);

    var options = {
        //colors: ['#fd0000','#43fa00','#0040ff'],
        sliceVisibilityThreshold: 0,
        hAxis: {title: 'Monat', titleTextStyle: {color: '#333333'}},
        vAxis: {
            minValue: 0,
            title: 'MWh',
        },
    };

    let chart = new google.visualization.PieChart(document.getElementById('chart_div_pieAccumulated'));
    chart.draw(data, options);
}

function drawLine() {
    var data = google.visualization.arrayToDataTable([
        ['Year', '2012', '2013', '2014'],
        ['Januar', 128.52, 144.44, 113.77],
        ['Februar', 187.54, 133.07, 96.17],
        ['März', 63.16, 121.78, 77.74],
        ['April', 72.18, 63.86, 38.27],
        ['Mai', 0, 35.53, 29.79],
        ['Juni', 0, 18.57, 11.6],
        ['Juli', 0, 0, 0],
        ['August', 0, 0, 0],
        ['September', 6, 16.56, 16.58],
        ['Oktober', 68.58, 48.53, 36.14],
        ['November', 84.54, 91.88, 67.55],
        ['Dezember', 120.51, 115.2, 138],

    ]);

    var options = {
        //colors: ['#fd0000','#43fa00','#0040ff'],
        hAxis: {title: 'Monat', titleTextStyle: {color: '#333333'}},
        vAxis: {
            minValue: 0,
            title: 'MWh',
        },
    };

    let chart = new google.visualization.LineChart(document.getElementById('chart_div_line'));
    chart.draw(data, options);
}

function drawScatter() {
    var data = google.visualization.arrayToDataTable([
        ['Year', '2012', '2013', '2014'],
        ['Januar', 128.52, 144.44, 113.77],
        ['Februar', 187.54, 133.07, 96.17],
        ['März', 63.16, 121.78, 77.74],
        ['April', 72.18, 63.86, 38.27],
        ['Mai', 0, 35.53, 29.79],
        ['Juni', 0, 18.57, 11.6],
        ['Juli', 0, 0, 0],
        ['August', 0, 0, 0],
        ['September', 6, 16.56, 16.58],
        ['Oktober', 68.58, 48.53, 36.14],
        ['November', 84.54, 91.88, 67.55],
        ['Dezember', 120.51, 115.2, 138],

    ]);

    var options = {
        //colors: ['#fd0000','#43fa00','#0040ff'],
        hAxis: {title: 'Monat', titleTextStyle: {color: '#333333'}},
        vAxis: {
            minValue: 0,
            title: 'MWh',
        },
    };

    let chart = new google.visualization.ScatterChart(document.getElementById('chart_div_scatter'));
    chart.draw(data, options);
}

function drawBubble() {
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Monat', 'MWh', 'Jahr', 'Prozent/Jahr'],
        ['Januar', 1, 128.52, '2012', 20.5],
        ['Januar', 1, 144.44, '2013', 19.8],
        ['Januar', 1, 113.77, '2014', 14.9],
        ['Februar', 2, 187.54, '2012', 29.9],
        ['Februar', 2, 133.07, '2013', 18.2],
        ['Februar', 2, 96.17, '2014', 12.6],
        ['März', 3, 63.16, '2012', 10.1],
        ['März', 3, 121.78, '2013', 16.7],
        ['März', 3, 77.74, '2014', 10.1],
        ['April', 4, 72.18, '2012', 11.5],
        ['April', 4, 63.86, '2013', 8.7],
        ['April', 4, 38.27, '2014', 5],
        ['Mai', 5, 0, '2012', 0],
        ['Mai', 5, 35.53, '2013', 4.9],
        ['Mai', 5, 29.79, '2014', 3.9],
        ['Juni', 6, 0, '2012', 0],
        ['Juni', 6, 18.57, '2013', 2.5],
        ['Juni', 6, 11.6, '2014', 1.5],
        ['Juli', 7, 0, '2012', 0],
        ['Juli', 7, 0, '2013', 0],
        ['Juli', 7, 0, '2014', 0],
        ['August', 8, 0, '2012', 0],
        ['August', 8, 0, '2013', 0],
        ['August', 8, 0, '2014', 0],
        ['September', 9, 6, '2012', 0.1],
        ['September', 9, 16.56, '2013', 2.3],
        ['September', 9, 16.56, '2014', 2.2],
        ['Oktober', 10, 68.58, '2012', 11],
        ['Oktober', 10, 48.53, '2013', 6.6],
        ['Oktober', 10, 36.14, '2014', 4.7],
        ['November', 11, 84.54, '2012', 13.5],
        ['November', 11, 91.88, '2013', 12.6],
        ['November', 11, 67.55, '2014', 8.8],
        ['Dezember', 12, 120.51, '2012', 19.3],
        ['Dezember', 12, 115.2, '2013', 15.8],
        ['Dezember', 12, 138, '2014', 18.1],
    ]);

    var options = {
        //colors: ['#fd0000','#43fa00','#0040ff'],
        hAxis: {
            title: 'Months',
            titleTextStyle: {color: '#333333'},
            ticks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
        },
        explorer: {},
        vAxis: {
            minValue: -5,
            maxValue: 205,
            title: 'MWh',
        },
    };

    let chart = new google.visualization.BubbleChart(document.getElementById('chart_div_bubble'));
    chart.draw(data, options);
}


$(window).resize(function () {
    drawPieYear();
    drawPieAccumulated();
    drawLine();
    drawScatter();
    drawBubble();
});
