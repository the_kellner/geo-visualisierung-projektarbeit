function generateTreeMap() {

    let rawData = [
        [
            {folder: "Ordner 1", filename: "Datei 1", size: 90, type: "DOC"},
            {folder: "Ordner 1", filename: "Datei 2", size: 60, type: "PNG"},
            {folder: "Ordner 1", filename: "Datei 3", size: 30, type: "PDF"},
        ],
        [
            {folder: "Ordner 2", filename: "Datei 4", size: 40, type: "DAT"},
            {folder: "Ordner 2", filename: "Datei 5", size: 20, type: "DAT"}
        ]
    ];

    let sizes = [
        rawData[0].map(value => value.size),
        rawData[1].map(value => value.size)
    ];

    let filenames = [
        rawData[0].map(value => value.filename),
        rawData[1].map(value => value.filename)
    ];

    Treemap.draw("treemap", 600, 400, sizes, filenames);

    function pickColor(coordinates, index) {
        console.log(index)
        let filetype = rawData[index[0]][index[1]].type
        switch (filetype) {
            case "DAT":
                return {"fill": "#b17d00"}
            case "DOC":
                return {"fill": "#1f3d98"}
            case "PDF":
                return {"fill": "#992020"}
            case "PNG":
                return {"fill": "#509625"}
        }
    }

    Treemap.draw("treemap_colored", 600, 400, sizes, filenames, {"box": pickColor});
}

$(document).ready(function () {
    $("#myToast").toast('show');
});

$(document).ready(function () {
    generateTreeMap()
});
